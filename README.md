SE Basic IV "Anya" fork by Shaos
================================

Forked on May 27th, 2013 from https://github.com/cheveron/sebasic4 (GitHub of Andrew Owen).

![Anya](https://gitlab.com/retro-forks/sebasic4/raw/master/images/anya.jpg)

Use **se-0.rom** and **se-1.rom** in place of the default Spectrum SE ROMs in the FUSE
or EightyOne emulators.

Alternatively, use **se-1.rom** as a standalone replacement 16K ROM in real
hardware or emulators.

See [Wiki] for details.

[Wiki]: https://gitlab.com/retro-forks/sebasic4/wikis/home

![KBD](https://gitlab.com/retro-forks/sebasic4/raw/master/images/keyboard.png)

Copyright notice from the source code:
<pre>
 SE BASIC IV
 Copyright (c) 2012 Andrew Owen. All rights reserved.

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation, either version 2 of the License, or (at your option) any later
 version. This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see http://www.gnu.org/licenses/.

 This software is based on OpenSE BASIC 
 http://sourceforge.net/projects/sebasic/ and contains portions of the
 following GPLv2 licensed software:

 ZX81 ROM, copyright (c) 1981 Nine Tiles Networks Ltd.
 SAM BASIC, copyright (c) 1989-1990 Andrew Wright.
</pre>

You can look at the history of code changes [here](docs/ChangeLog)

Original list of features of v4.0.4:

-    128 RAM cleared on hard reset.
-    15 new commands.
-    3 new functions.
-    7-bit character support: UDG 7.
-    8-bit character set support: UDG 8.
-    8-bit mode can be used to provide an additional 128 UDGs.
-    80 column text: MODE 1.
-    Abbreviated command entry: GO. 100.
-    All symbols accessed via symbol shift only.
-    ANSI BASIC-78 / ECMA-55 Minimal BASIC standard compliant.
-    Assembly compiles with GNU binutils.
-    Assembly file includes X80 virtual co-processor definitions.
-    AY silenced on BASIC report.
-    AY supported on Timex and 128 ports.
-    AY-3-891x support: SOUND 1,2;3,4.
-    Based on the ZX81 ROM by John Grant & Steve Vickers, who wrote the Spectrum ROM.
-    Built-in support for Spectranet syntax.
-    CALL command for calling machine code.
-    Chloe font specially designed for maximum legibility in hi-res mode.
-    CLEAR performs a RESTORE.
-    CLUT command allows in-line changing of CLUTs.
-    CLUT command includes old BRIGHT functionality.
-    COLOR command enables in-line setting of attributes.
-    Cursor down repeatedly to jump to the end of a line.
-    Cursor jumps to an error in the line.
-    Cursor up repeatedly to jump to the start of a line.
-    Cursors can move up and down in an edit line.
-    Decimal to hexadecimal string function: ~9
-    Default high-contrast white on black display.
-    Default ULAplus palette.
-    Designed specifically for the Chloe 280SE.
-    Detokenizer simplifies editing lines.
-    Edit any line: EDIT 10
-    Enhanced floating point library.
-    Enter tab stops directly.
-    Error escaping: ON ERROR STOP.
-    Error skipping: ON ERROR CONTINUE.
-    Error trapping: ON ERROR GOTO 100.
-    Extend mode is not required.
-    FAST command sets CPU to full speed.
-    Fast lo-res text printing.
-    Fast pilot signal sync.
-    Fast reset.
-    Four channel sound possible by combining BEEP and SOUND commands.
-    Full support for BASIC editing in hi-res mode.
-    Full support for block graphics in hi-res mode.
-    Full support for UDGs in hi-res mode.
-    Fully open source (GPLv2 licensed).
-    Geneva Mono font suitable for 32 or 40 column lo-res displays.
-    Hexadecimal to decimal function: &F.
-    Includes legally machine reverse engineered replacements for critical routines.
-    Incorporates elements of SAM BASIC by Andy Wright, creator of BETA BASIC.
-    Increased line range: 0-16383.
-    Invalid lines generate an error sound.
-    INVERSE support in 80 column mode.
-    Inverted cursor for Timex hi-res.
-    IY register available in interrupt mode 1
-    Key click is silent by default.
-    Key response is mode dependent.
-    Keyword mode is not required.
-    Load ULAplus screens with LOAD "" CODE 16384: RESET.
-    Lower 16K ROM can be used on its own in all existing Sinclair, Timex and Amstrad models.
-    Maintains support for software that uses ROM IM2 vectors with well-behaved hardware.
-    Maintains support for software that uses the first two bytes of the ROM as a JP instruction.
-    More room for BASIC programs.
-    Newton-Raphson square root function.
-    No keyword locations to memorize.
-    No redundant legacy code.
-    Not affected by more than 40 bugs that were present in the original ROM.
-    Number codes dropped from error reports.
-    Octal to decimal function: /7.
-    Only 32K ROM supported by devices that force USR 0 mode.
-    Optimized for speed.
-    OVER support in 80 column mode.
-    PAPER sets color pair in hi-res mode.
-    PAUSE defaults to PAUSE 0 without a parameter.
-    PEN sets color pair in hi-res mode.
-    Prevents accidental deletion of edit line on older keyboards.
-    Quick entry for common functions.
-    Quick entry for most common tape commands.
-    Quick start guide, including memory map, system variables, flags, and command summary.
-    Recover from crashes with NMI.
-    Remove multiple lines: DELETE 10,100.
-    Renumber with step: RENUM 5,5.
-    Reset ULAplus palette from buffer: RESET.
-    Retained support for embedding control codes.
-    SAM style cursors.
-    Save ULAplus screens with SAVE "screen" 16384, 6976
-    SCREEN$() supports UDGs and 8-bit character sets.
-    Scroll counter increased.
-    Set the permanent attribute: COLOR 7.
-    Sets the standard video mode on reset.
-    Shift in CAPS mode gives lower case letters.
-    Simple code page support in hi-res mode.
-    Simplified error messages.
-    Simplified flags.
-    Simplified key tables.
-    Single code-base for lo-res and hi-res ROMs.
-    SLOW command sets CPU to 3.5Mhz.
-    STOP token accessible during INPUT with cursor down.
-    Supports a large number of legacy devices.
-    Supports most existing programs in lo-res mode.
-    Supports some existing programs in hi-res mode.
-    Switching to INSERT mode before entering a line prevents tokenizing lower case characters.
-    Text after a REM statement is not tokenized.
-    Tokenized command entry.
-    ULAplus palette buffer following the default screen area.
-    ULAplus support: PALETTE 64,1.
-    Use REM at the start of a line to clear an unwanted edit line.
-    Use reserved words as variable names during tokenization.
-    User system variable space accessible via IY register.
-    Works with esxDOS on divIDE.
-    Z80 LDIR block copy: PUT 49152, 16384, 6912.
